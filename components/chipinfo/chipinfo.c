#include "stdio.h"
#include "chipinfo.h"
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

void get_chip_info(void)
{
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    printf("chip: %s\n", CONFIG_IDF_TARGET); 
    printf("CPU Core(s): %d\n", chip_info.cores);
    printf("wireless: WiFi %s %s\n", (chip_info.features & CHIP_FEATURE_BT) ? "BT" : "", (chip_info.features & CHIP_FEATURE_BLE) ? "BLE" : "");
    printf("silicon revision: %d\n", chip_info.revision);
    printf("flash: %dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024), (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "axternal");
    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
}
