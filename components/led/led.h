#ifndef __LED_H
#define __LED_H          

#define BLINK_GPIO    2

void led_init(void);
void led_on(void);
void led_off(void);
void led_blink(void);

#endif