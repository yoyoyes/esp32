/*
    此示例展示如何使用全频道扫描或快速扫描连接到 WiFi 网络。 
    在快速扫描模式下，一旦找到与 SSID 匹配的第一个网络，扫描就会停止。 在这种模式下，应用程序可以设置认证模式和信号强度的阈值。 不满足阈值要求的网络将被忽略。 
    在全频道扫描模式下，只有在扫描完所有频道后，扫描才会结束，并从最好的网络开始连接。 可以根据身份验证模式或信号强度对网络进行排序。 认证模式的优先级为：WPA2 > WPA > WEP > Open 
 */

#include "fastscan.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event.h"
#include "nvs_flash.h"

#define WIFI_SSID       "Redmi"
#define WIFI_PASSWORD   "lin111983"


static const char *TAG = "scan";

/* wifi 触发事件 */
static void wifi_event_handler(void *arg, esp_event_base_t event_base, 
                               int32_t event_id, void *event_data)
{
    if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        esp_wifi_connect();
    }
    else if(event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    }
}

/* 初始化 wifi 为 STA 并设置 快速扫描方式 */
static void wifi_fast_scan(void)
{
    ESP_ERROR_CHECK(esp_netif_init());                      /* esp 网络初始化 */
    ESP_ERROR_CHECK(esp_event_loop_create_default());       /* esp 创建默认事件循环 */
    esp_netif_create_default_wifi_sta();                    /* 初始化 wifi 为 STA */

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();    /* 配置 wifi 默认参数 */
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));                   /* wifi 初始化配置 */

    /* 注册 wifi 事件 */
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, 
                                                        ESP_EVENT_ANY_ID, 
                                                        &wifi_event_handler, 
                                                        NULL, 
                                                        NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));
    
    /* 配置 wifi */
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASSWORD,              
            .scan_method = WIFI_FAST_SCAN,                  /* 快速扫描方式 */
            .sort_method = WIFI_CONNECT_AP_BY_SECURITY,     /* 安全认证模式 */
            .threshold.rssi = -127,                         /* 信号强度阈值 */
            .threshold.authmode = WIFI_AUTH_OPEN            /* 自动模式 */
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));                  /* 设置 wifi 为 STA 模式 */
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));    /* 设置 wifi 配置 */
    ESP_ERROR_CHECK(esp_wifi_start());                                  /* 开启 wifi */
}

/* esp32 wifi fast scan demo */
void esp32_wifi_fast_scan_test(void)
{
    esp_err_t ret = nvs_flash_init();
    if(ret == ESP_ERR_NVS_NO_FREE_PAGES || ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ESP_ERROR_CHECK(nvs_flash_init());
    }

    wifi_fast_scan();
}